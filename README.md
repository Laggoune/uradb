# UraDb #
## documentation ##

first go to the Database.php file and edit it and change the **host user pass**

## How to insert ##

```
#!php

require 'Database.php';
$db = new DB;

$table 		= "users";
$columns 	= ['username','password'];
$values 	= ['foo','123'];
$db->save($table,$columns,$values);

```

## How to select ##

```
#!php

require 'Database.php';
$db 	= new DB;
$table 	= 'users';

foreach($db->get($table)->all() as $data)
{
	echo $data->id; 
	/...
}

```

## How to select special data ##

```
#!php

require 'Database.php';
$db 	= new DB;
$table 	= 'users';

print_r($db->get($table)->where("id","=","1"));

```
good luck x)
